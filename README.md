# kwin-toggle-window

Simple script to toggle the visibility of a window by (un-)minimizing it.  
The relevant window is selected by searching for a specific prefix (default: Alacritty).

## Configuration

The window name can be changed like this:
```
kwriteconfig5 --file ~/.config/kwinrc --group Script-togglewindow --key WindowName <window title>
```
Restart KWin after changing the configuration.
