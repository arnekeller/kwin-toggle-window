/*
	SPDX-FileCopyrightText: 2024 Arne Keller <arne.keller@posteo.de>
	SPDX-License-Identifier: GPL-3.0-or-later
*/

var windowName = null;

function isRelevant(client) {
	// window caption should start with the specified string
	return client.caption.indexOf(windowName) === 0;
}

function toggleWindow() {
	assertTrue(windowName != null, "ToggleWindow failed to load windowName from config!");
	var allClients = workspace.windowList();
	for (var i = 0; i < allClients.length; i++) {
		if (isRelevant(allClients[i])) {
			var prevMini = allClients[i].minimized;
			allClients[i].minimized = !prevMini;
			if (prevMini) {
				workspace.activeWindow = allClients[i];
			}
			break; // only toggle one window
		}
	}
}

function init() {
	windowName = readConfig("WindowName", "Alacritty").toString();
}

options.configChanged.connect(init);

init();
registerShortcut("ToggleWindow", "Toggle Window", "Meta+F1", toggleWindow);
